# Requirement

## `jira` VS `clickup`

||`jira`|`clickup`|
|:--|:--:|:--:|
|app chat in `slack`|✅|❌|
|integration issue in `gitlab`|❓|❌|
