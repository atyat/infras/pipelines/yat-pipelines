# Jira-Cloud Usage Guide

## Settings

1. `project settings >>> issue types`的各个`issue types`增加一个`description fields`
    1. `field type`: paragraph
    1. `field name`: Acceptance Criteria
    1. `field description`: what criteria should be satisfied before accept this issue
