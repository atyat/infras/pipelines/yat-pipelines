# Product-Owner Guides

## Requirement Engineering with `jira`

1. 将需求切分成若干个`epic`
1. 在`epic`中创建`story`
    1. `story`可以再划分为`sub-task`
1. 将`story`调度到某个`sprint`
1. 为每个`story`创建一个`git branch`进行实施，该`git branch`合并到`default-branch`里关闭该`story`

❗ ❗ ❗

1. `jira cloud`中`sprint`以`story`为调度单元，`epic`和`sub-task`无法在`sprint`作为调度单元
1. 未找到`jira cloud`进行`issue split`的方法

❗ ❗ ❗

>1. [atlassian.com. Create an issue and a sub-task.](https://support.atlassian.com/jira-software-cloud/docs/create-an-issue-and-a-sub-task/#Split-an-issue)
