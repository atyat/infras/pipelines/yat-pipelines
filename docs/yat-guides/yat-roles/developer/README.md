# Developer Guides

## Cooperate `jira` and `gitlab`

1. `jira` ---> `gitlab`:
    1. create `branch`, `commit`, `merge request` base on `jira issue`
    1. retrieve `branch`, `commit`, `merge request` in `jira development panel`
1. `jira` <--- `gitlab`:
    1. `merge request code review` linking to `jira issue`
    1. `git commit` transit `jira issue` status
    1. `merge request code review` close `jira issue`

>1. [gitkraken.com. Git Integration for Jira Cloud Documentation.](https://help.gitkraken.com/git-integration-for-jira-cloud/git-integration-for-jira-home-gij-cloud/)
>1. [gitkraken.com. Linking git commits to Jira issues.](https://help.gitkraken.com/git-integration-for-jira-cloud/linking-git-commits-to-jira-issues-gij-cloud/)
>1. [atlassian.com. Reference issues in your development work.](https://support.atlassian.com/jira-software-cloud/docs/reference-issues-in-your-development-work/)
>1. [atlassian.com. Enable Smart Commits.](https://support.atlassian.com/jira-cloud-administration/docs/enable-smart-commits/)
>1. [atlassian.com. Process issues with smart commits.](https://support.atlassian.com/jira-software-cloud/docs/process-issues-with-smart-commits/)
