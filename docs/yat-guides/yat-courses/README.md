# Guideline in Courses

## `yat-pipelines`相关的课程及角色

![course relative](./.assets/diagram/crse_devops_v2.png)

1. 软件工程概论：`ea`, `visio`
1. 软件项目管理：`visio`, `project`
1. Linux系统管理与自动化运维：`linux`，`ansible`/`puppet`/`chef`
1. 云原生与容器技术：`docker`、`kubernetes`、`prometheus`
1. 软件前端框架技术：`html + css + javascript`，`vuejs`/`reactjs`/`angularjs`
1. 软件后端框架技术：`spring frameworks`, `orm frameworks`
1. 软件需求分析：`slack`, `jira`, `confluence`，`git`, `plantuml`
1. 软件质量保证与测试：`maven`，`junit`，`sonar` ,`jmeter`，`selenium`，`gitlab ci`/`jenkins`, `bugzilla`
1. 软件工程经济学：`clickup`, `trello`, `jira plugins`
1. 软件系统设计与体系结构：`front-end and back-end separation`, `spring cloud`, `cloud native`
1. 软件工程综合实践：`yat-pipelines`

<!--
|课程|角色|工具|
|--|--|--|
|软件工程概论|
-->
