# YAT-Pipelines Guides

```bash {.line-numbers cmd=true output=text hide=true run_on_save=true}
tree -d .
```

```bash {.line-numbers}
.
├── courses
├── integrations
│   ├── atlassian
│   ├── gitlab
│   ├── jetbrains
│   ├── slack
│   ├── sonar
│   └── vscode
├── roles
│   ├── assurer
│   ├── developer
│   ├── operator
│   ├── product-owner
│   └── tester
└── tools
    ├── atlassian
    ├── gitlab
    ├── jetbrains
    ├── slack
    ├── sonar
    └── vscode
```
