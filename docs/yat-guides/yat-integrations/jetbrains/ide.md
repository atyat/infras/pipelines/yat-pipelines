# JetBrains IDE Integrations

## `jira-cloud` <---> `jetbrains-ide` Integration

### `jira-cloud` <---> `jetbrains-ide` Integration Steps

1. create `atlassian api token`: `atlassian account >>> security >>> api token`
1. linking `jira-cloud` and `jetbrains-ide`( **valid in `project-scope`** ): `preferences >>> tools >>> task >>> servers`

## `gitlab` <---> `jetbrains` Integration

### `gitlab` <---> `jetbrains` Integration Steps
