# Pipelines Integration

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [Cautions](#cautions)
2. [Integrations](#integrations)
3. [Plans and Pricing](#plans-and-pricing)
4. [Some Special Integrations](#some-special-integrations)
    1. [`jira cloud` <--> `gitlab` Integration](#jira-cloud-gitlab-integration)
        1. [`jira cloud` <--> `gitlab` Integration Features](#jira-cloud-gitlab-integration-features)
        2. [`jira cloud` <--> `gitlab` Integration Methods](#jira-cloud-gitlab-integration-methods)

<!-- /code_chunk_output -->

## Cautions

1. 免费版一般不支持`SSO`，因此，建议各工具使用相同的`display name`/`nickname`和`avatar`，同时也使用相同的`full name`和`email`

## Integrations

1. `forward pipelines`(mainly `control`)
    1. `issue pipeline`: `confluence-cloud` ---> `jira-cloud` ---> {`local terminal` | `local ide`} ---> `gitlab`
    1. `testing pipeline`: {`local terminal` | `local ide`} ---> `gitlab` ---> `gitlab-ci` ---> {`gitlab-runner`, `sonar-cloud`}
    1. `deploy pipeline`: <!--TODO:-->
    1. `monitoring pipeline`: <!--TODO:-->
1. `backward pipelines`(mainly `feedback`/`notification`)
    1. `issue pipeline`: `confluence-cloud` <--- `jira-cloud` <--- `gitlab` <--- `local terminal` or `local ide`
    1. `testing pipeline`: `slack` <--- `gitlab` <--- `gitlab-ci` <--- {`gitlab-runner`, `sonar-cloud`}
    1. `deploy pipeline`: <!--TODO:-->
    1. `monitoring pipeline`: <!--TODO:-->

## Plans and Pricing

1. [`slack`](https://app.slack.com/plans/T03LE20TFNU)
    - 🆓 integrations with other apps <= 10
1. `atlassian`
    1. [`jira-cloud`](https://www.atlassian.com/software/jira/pricing)
        - 🆓 users <= 10
    1. [`jira-service-cloud`](https://www.atlassian.com/software/jira/service-management/pricing)
        - 🆓 user agents <= 3
    1. [`confluence-cloud`](https://www.atlassian.com/software/confluence/pricing)
        - 🆓 users <= 10
    1. [`opsgenie-cloud`](https://www.atlassian.com/software/opsgenie/pricing)
        - 🆓 users <= 5
1. [`gitlab`](https://about.gitlab.com/pricing/): `saas` and `on-premises`/`self-managed` is the same price
1. [`sonar-qube` & `sonar-cloud`](https://www.sonarsource.com/plans-and-pricing/)
    - 🆓 `sonar-cloud` for `open-source projects`
    - 💰 `sonar-cloud` for `private projects`
    - 💰 `sonar-qube`

>1. [gitlab.com. Pricing model.](https://about.gitlab.com/company/pricing/)

## Some Special Integrations

### `jira cloud` <--> `gitlab` Integration

#### `jira cloud` <--> `gitlab` Integration Features

>When an ID of a Jira issue is mentioned in GitLab, in a commit, branch, and or merge request, that activity will be visible in the Development panel in the mentioned Jira issue.
>
>Additionally, if the Jira integration is enabled for a project on GitLab.com, engineers can use specific commands in commit messages to:
>
>1. Close Jira issues from GitLab.com
>1. Add a comments to Jira issue from GitLab.com
>1. Transition Jira workflow states (e.g. from "In progress" to "Done")
>1. View a list of read-only Jira issues in GitLab.com
>
>>[`atlassian marketplace >>> gitlab.com integration for jira cloud`](https://marketplace.atlassian.com/apps/1221011/gitlab-com-for-jira-cloud?tab=overview&hosting=cloud)

❗ "View a list of read-only Jira issues in GitLab.com" need `premium subscription`, see details in [gitlab.com. View Jira issues.](https://docs.gitlab.com/ee/integration/jira/issues.html#view-jira-issues) ❗

#### `jira cloud` <--> `gitlab` Integration Methods

>GitLab offers two types of Jira integrations, and you can use one or both depending on the capabilities you need. **We recommend you enable both.**
>
>1. Jira integration: **configure the settings in `gitlab`**
>1. Jira development panel integration: **configure the settings in `jira`**, use the `GitLab.com for Jira Cloud app`(when use `jira cloud`) or the `Jira DVCS (distributed version control system)`(when use `jira on-premises`) connector
>
>>1. [gitlab.com. Jira integrations.](https://docs.gitlab.com/ee/integration/jira/)

❗ in `gitlab` documentation, "`jira-cloud` ---> `gitlab` integration"(use [`atlassian marketplace >>> gitlab.com integration for jira cloud`](https://marketplace.atlassian.com/apps/1221011/gitlab-com-for-jira-cloud?tab=overview&hosting=cloud)) is named as  [`Jira development panel integration`](https://docs.gitlab.com/ee/integration/jira/index.html#jira-development-panel-integration) ❗

1. jira integration (`jira-cloud` <--- `gitlab` ): see details in [here](./gitlab/gitlab.md)
1. jira development panel integration (`jira cloud` --> `gitlab`): see details in [here](./atlassian/jira-cloud.md)

<!--
## `feishu` and `jira cloud` Integration

1. 参考文档：
    1. [使用 Jira 大师.](https://www.feishu.cn/hc/zh-CN/articles/360045214614)

## `feishu` and `gitlab` Integration

1. 参考文档：
    1. [在飞书捷径中使用 GitLab.](https://www.feishu.cn/hc/zh-CN/articles/360040069394)
-->
