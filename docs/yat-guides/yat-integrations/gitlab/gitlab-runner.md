# GitLab-Runner

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [Run `gitlab-runner` in `docker`](#run-gitlab-runner-in-docker)
2. [Speed Up `gitlab-runner`](#speed-up-gitlab-runner)

<!-- /code_chunk_output -->

## Run `gitlab-runner` in `docker`

see example in [${this-repo-base}/codes/gitlab-runner/gitlab-runner-15.2.1-maven](../../../codes/gitlab-runner/gitlab-runner-15.2.1-maven/README.md)

## Speed Up `gitlab-runner`

1. [Caching in GitLab CI/CD.](https://docs.gitlab.com/ee/ci/caching/)
1. [Speed up job execution.](https://docs.gitlab.com/runner/configuration/speed_up_job_execution.html)
1. [Optimizing Maven Builds with Gitlab Docker Runner.](https://www.auroria.io/optimizing-maven-builds-with-gitlab-docker-runner/)
