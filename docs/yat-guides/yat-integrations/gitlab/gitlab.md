# GitLab

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [`slack` <--- `gitlab` Integration](#slack-gitlab-integration)
    1. [Integration Steps](#integration-steps)
2. [`jira cloud` <--- `gitlab` Integration](#jira-cloud-gitlab-integration)
    1. [`jira cloud` <--- `gitlab` Integration Features](#jira-cloud-gitlab-integration-features)
    2. [`jira cloud` <--- `gitlab` Integration Steps](#jira-cloud-gitlab-integration-steps)
    3. [`jira cloud` <--- `gitlab` Integration Usages](#jira-cloud-gitlab-integration-usages)

<!-- /code_chunk_output -->

## `slack` <--- `gitlab` Integration

### Integration Steps

>1. [gitlab.com. Slack notifications service.](https://docs.gitlab.com/ee/user/project/integrations/slack.html)
>1. [slack.com. Sending messages using Incoming Webhooks.](https://api.slack.com/messaging/webhooks)
>1. [slack.com/apps. Incoming WebHooks.](https://slack.com/apps/A0F7XDUAZ-incoming-webhooks?tab=more_info) ⚠️ deprecated, use ["slack.com. Sending messages using Incoming Webhooks."](https://api.slack.com/messaging/webhooks) instead ⚠️

## `jira cloud` <--- `gitlab` Integration

>1. [准时下班的秘密：集成GitLab && JIRA 实现自动化工作流.](https://www.cnblogs.com/xiao2shiqi/p/13514548.html)

### `jira cloud` <--- `gitlab` Integration Features

❗ in `gitlab` documentation, "`jira cloud` <--- `gitlab` integration" is named as [`jira integration`](https://docs.gitlab.com/ee/integration/jira/index.html#jira-integration) ❗

1. Mention a `Jira issue ID` in a GitLab commit or merge request, and a link to the Jira issue is created.
1. Mention a `Jira issue ID` in GitLab and the Jira issue shows the GitLab issue or merge request.
1. Mention a `Jira issue ID` in a GitLab commit message and the Jira issue shows the commit message.
1. Display a list of Jira issues. ( see more in ["View Jira issues"](https://docs.gitlab.com/ee/integration/jira/issues.html#view-jira-issues) ⚠️ requires a `gitlab premium plan` ⚠️ )
1. Create a Jira issue from a vulnerability or finding. ( see more in ["Create a Jira issue for a vulnerability"](https://docs.gitlab.com/ee/user/application_security/vulnerabilities/index.html#create-a-jira-issue-for-a-vulnerability) )

>1. [gitlab.com. Jira integrations.](https://docs.gitlab.com/ee/integration/jira/)

### `jira cloud` <--- `gitlab` Integration Steps

see more in "[Configure the Jira integration in GitLab.](https://docs.gitlab.com/ee/integration/jira/configure.html)"

1. `gitlab namespace >>> settings >>> integrations >>> jira`
    1. connection details
        1. `web url`: `jira cloud`的二级域名链接，如`https://xxx.atlassian.net`
        1. `jira api url`: `jira cloud`留空即可
        1. `username or email`: `jira cloud`管理员用户名或邮箱账号
        1. `enter new password or api token`: `jira cloud`管理员用户密码或`api token`（创建`jira cloud api token`参见"[Create an API token for Jira in Atlassian cloud.](https://docs.gitlab.com/ee/integration/jira/jira_cloud_configuration.html)"）
    1. trigger: when a `Jira issue` is mentioned in a commit or merge request, a remote link and comment (if enabled) will be created.
        1. `commit`
        1. `merge request`
            1. `comment`
            1. transition `jira issues` to their `final state`

>1. [gitlab.com. Configure the Jira integration in GitLab.](https://docs.gitlab.com/ee/integration/jira/configure.html)
>1. [gitlab.com. Create an API token for Jira in Atlassian cloud.](https://docs.gitlab.com/ee/integration/jira/jira_cloud_configuration.html)

### `jira cloud` <--- `gitlab` Integration Usages

1. 在`git commit`和`gitlab merge request >>> title or description`中通过`jira issue id`进行关联
