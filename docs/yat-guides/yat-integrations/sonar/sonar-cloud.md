# Sonar-Cloud

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

## `gitlab-ci` <---> `sonar-cloud` Integration

### `gitlab-ci` <---> `sonar-cloud` Integration Features

### `gitlab-ci` <---> `sonar-cloud` Integration Steps

1. create a new `analyze project` in `sonar-cloud`:
    1. `sonar-cloud >>> top navigator bar >>> + >>> analyze a new project`
    1. `choose an organization` or `import another organization` (`organization` in `sonar-cloud` is correspond to `group` in `gitlab`)
    1. select the `gitlab project`
1. add `SONAR_HOST_URL` environment variable in `gitlab` (can add to the `group` or `project` in `gitlab`)
    1. `${group} or ${project} >>> settings >>> ci/cd >>> variables`, `key` ==> `SONAR_HOST_URL`, `value` ==> `https://sonarcloud.io` , `protect variable` ==> unchecked, `mask variable` ==> unchecked or checked would be ok
1. define and add `SONAR_TOKEN` environment variable
    1. define the `SONAR_TOKEN` in `sonar-cloud`: `my account >>> security > generate tokens`
    1. add the `SONAR_TOKEN` in `gitlab`: `${project} >>> settings >>> ci/cd >>> variables`, `key` ==> `SONAR_TOKEN`, `value` ==> the token value, `protect variable` ==> unchecked, `mask variable` ==> checked
1. update the `gitlab-ci.yml` and `building-script`: see detail in [sonarcloud.io. Analyze Your Repository With GitLab CI.](https://docs.sonarcloud.io/advanced-setup/ci-based-analysis/gitlab-ci/)

>1. [sonarcloud.io. Analyze Your Repository With GitLab CI.](https://docs.sonarcloud.io/advanced-setup/ci-based-analysis/gitlab-ci/)
