# Jira-cloud

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [`slack` <---> `jira-cloud` Integration](#slack-jira-cloud-integration)
2. [`jira-cloud` <---> `confluence-cloud` Integration](#jira-cloud-confluence-cloud-integration)
    1. [`jira-cloud` <---> `confluence-cloud` Integration Steps](#jira-cloud-confluence-cloud-integration-steps)
3. [`jira-cloud` ---> `gitlab` Integration](#jira-cloud-gitlab-integration)
    1. [`jira-cloud` ---> `gitlab` Integration Features](#jira-cloud-gitlab-integration-features)
    2. [`jira-cloud` ---> `gitlab` Integration Steps](#jira-cloud-gitlab-integration-steps)

<!-- /code_chunk_output -->

## `slack` <---> `jira-cloud` Integration

see details in [this page](../slack/slack.md)...

## `jira-cloud` <---> `confluence-cloud` Integration

### `jira-cloud` <---> `confluence-cloud` Integration Steps

`jira-cloud`与`confluence-cloud`之间的集成主要通过：

1. `atlassian inner integrated`： `atlassian software`已集成的`navigator`
1. `self-defined link integration`: 添加的`link`进行集成

`atlassian software`已集成的`navigator`包括（但不限于）：

具体的集成方式包括（但不限于）以下：

1. `top navigator >>> switch to ...` (`atlassian inner integrated`)
1. `project <--> space`：`jira-cloud project`和`confluence-cloud space`之间的`link`
    - `confluence-cloud shortcut` to a `jira-cloud project`
    - `jira-cloud >>> left side bar navigator >>> project pages`(`atlassian inner integrated`)
1. `project <--> page`：
    - `jira-cloud shortcut` to a `confluence-cloud page`
1. `issue <--> page`:
1. `page content --> project/board/issue`

>[atlassian. Tutorial: Using Confluence Cloud and Jira Software Cloud together.](https://www.atlassian.com/software/confluence/guides/expand-confluence/confluence-and-jira#Use-embedded-Confluence-pages-in-Jira)

## `jira-cloud(software)` <---> `jira-cloud(service)` Integration

>1. [atlassian.com. How Jira Service Management and Jira Software work together.](https://www.atlassian.com/software/jira/service-management/product-guide/tips-and-tricks/jira-service-management-and-jira-software#overview)

## `jira-cloud` ---> `gitlab` Integration

### `jira-cloud` ---> `gitlab` Integration Features

>GitLab offers two types of Jira integrations, and you can use one or both depending on the capabilities you need. We recommend you enable both.
>
>1. Jira integration: configure the settings in `gitlab`
>1. Jira development panel integration: use the `GitLab.com for Jira Cloud app`(when use `jira cloud`) or the `Jira DVCS (distributed version control system)`(when use `jira on-premises`) connector, and configure the settings in `jira`

❗ in `gitlab` documentation, "`jira-cloud` ---> `gitlab` integration" is named as  [`Jira development panel integration`](https://docs.gitlab.com/ee/integration/jira/index.html#jira-development-panel-integration) ❗

>1. [gitlab.com. Jira integrations.](https://docs.gitlab.com/ee/integration/jira/)

### `jira-cloud` ---> `gitlab` Integration Steps

1. `jira-cloud`安装`gitlab integration`：在`atlassian marketplace`搜索并安装[`GitLab.com for Jira Cloud`](https://marketplace.atlassian.com/apps/1221011/gitlab-com-for-jira-cloud?hosting=cloud&tab=overview)
1. `jira`关联`gitlab account`：按插件的提示关联`gitlab`的账号
1. `jira`关联`gitlab namespace`：`apps >>> manage your apps >>> gitlab for jira(gitlab.com) >>> get started >>> add namespace`
<!-- 1. `jira`关联`gitlab repo`： -->
1. `jira project`配置`workflow`：

>1. [atlassian.com. Integrate GitLab with Jira.](https://support.atlassian.com/jira-cloud-administration/docs/integrate-gitlab-with-jira/)
>1. [gitlab.com. GitLab Jira development panel integration](https://docs.gitlab.com/ee/integration/jira/development_panel.html)
>1. [Configure GitLab Jira Integration using Marketplace App.](https://www.youtube.com/watch?v=SwR-g1s1zTo)
