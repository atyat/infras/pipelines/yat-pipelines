# Confluence Cloud Integration

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [`slack` <---> `confluence-cloud` Integration](#slack-confluence-cloud-integration)
2. [`jira-cloud` <---> `confluence-cloud` Integration](#jira-cloud-confluence-cloud-integration)

<!-- /code_chunk_output -->

## `slack` <---> `confluence-cloud` Integration

see details in [this page](../slack/slack.md)...

## `jira-cloud` <---> `confluence-cloud` Integration

see details in [this page](./jira-cloud.md)...
