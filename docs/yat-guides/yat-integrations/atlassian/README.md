# Integrate Atlassian Family

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=3 orderedList=true} -->

<!-- code_chunk_output -->

1. [Cautions](#cautions)
2. [Members](#members)

<!-- /code_chunk_output -->

## Cautions

❗ ❗ ❗<!--#region caution-->

1. `atlassian`的产品与`safari`存在兼容性问题，建议使用`chrome`
1. `gitlab.com integration for jira cloud`安装完后显示的名称是`gitlab for jira(gitlab.com)`

❗ ❗ ❗<!--#endregion caution-->

## Members

1. `jira-cloud`
    1. `atlassian marketplace >>> gitlab.com integration for jira cloud`
1. `confluence-cloud`

<!--

## `trello`

>[atlassian. Jira Software vs Trello.](https://www.atlassian.com/software/jira/comparison/jira-vs-trello)

### `trello` and `confluence cloud` Integration

>[atlassian.Use Trello and Confluence together.](https://support.atlassian.com/confluence-cloud/docs/use-trello-and-confluence-together/)

-->
