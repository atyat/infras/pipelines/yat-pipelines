# VSCode Integration

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [`jira-cloud` <---> `vscode` Integration](#jira-cloud-vscode-integration)
    1. [`jira-cloud` <---> `vscode` Integration Steps](#jira-cloud-vscode-integration-steps)

<!-- /code_chunk_output -->

## `jira-cloud` <---> `vscode` Integration

### `jira-cloud` <---> `vscode` Integration Steps

1. install `vscode-extension`: `vscode >>> extensions` install `Jira and Bitbucket (Atlassian Labs)`
1. follow the `Jira and Bitbucket (Atlassian Labs)`'s instructions
