# Slack Family

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [Members](#members)

<!-- /code_chunk_output -->

## Members

1. `slack`
    1. `slack apps >>> jira cloud`
    1. `slack apps >>> confluence cloud`
    1. `slack apps >>> gitlab` ( optional, not really useful )
    1. `slack apps >>> incoming webhooks`( optional, deprecated by `slack` )
