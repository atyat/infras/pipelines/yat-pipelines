# Slack Integration

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=3 orderedList=true} -->

<!-- code_chunk_output -->

1. [`slack` <---> `jira cloud` Integration](#slack-jira-cloud-integration)
    1. [`slack` <---> `jira cloud` Integration Features](#slack-jira-cloud-integration-features)
    2. [`slack` <---> `jira cloud` Integration Steps](#slack-jira-cloud-integration-steps)
2. [`slack` <---> `confluence-cloud` Integration](#slack-confluence-cloud-integration)
    1. [`slack` <---> `confluence-cloud` Integration Features](#slack-confluence-cloud-integration-features)
    2. [`slack` <---> `confluence-cloud` Integration Steps](#slack-confluence-cloud-integration-steps)
3. [`slack` ---> `gitlab` Integration](#slack-gitlab-integration)
    1. [`slack` ---> `gitlab` Integration Features](#slack-gitlab-integration-features)
    2. [`slack` ---> `gitlab` Integration Steps](#slack-gitlab-integration-steps)

<!-- /code_chunk_output -->

## `slack` <---> `jira-cloud` Integration

### `slack` <---> `jira-cloud` Integration Features

1. `slack` ---> `jira-cloud`( **instruction** )
    1. `slack >>> workspace >>> ${channel}` && `slack >>> workspace >>> apps >>> jira`：发出`/jira xxx`指令对`jira-cloud`进行操作（输入`/jira help`查看帮助）
    1. `slack >>> workspace >>> ${channel}` && `slack >>> workspace >>> apps >>> jira`：对接受到的`notification`进行操作（点击`message`的`...`选择操作类型）
1. `slack` <--- `jira-cloud`( **notification** )
    1. `slack >>> workspace >>> apps >>> jira`中订阅`jira-cloud`的`notification`
    1. `slack >>> workspace >>> ${channel}`中订阅`jira-cloud project`的`notification`

### `slack` <---> `jira-cloud` Integration Steps

1. linking `slack workspace` and `jira-cloud site`：在`slack >>> workspace >>> apps`搜索并安装`jira-cloud`，按指引关联`slack workspace`和`jira-cloud site`
1. linking `slack channel` and `jira-cloud project`
    - 方法01：`slack >>> workspace >>> channel` || `slack >>> workspace >>> apps >>> jira`输入`/jira connect`将`slack channel`和`jira-cloud project`进行关联
    - 方法02：`jira-cloud >>> ${project} >>> project settings >>> apps >>> slack integration`中将`slack channel`和`jira-cloud project`进行关联

>1. [atlassian.com. Integrate Jira Cloud and Slack.](https://support.atlassian.com/jira-work-management/docs/integrate-jira-cloud-and-slack/)
>1. [slack.com. An easier way to use JIRA with Slack.](https://slack.com/blog/productivity/an-easier-way-to-use-jira-with-slack)
>1. [atlassian.com. Integrate Jira Software Cloud and Slack.](https://www.atlassian.com/software/jira/guides/expand-jira/jira-slack-integration)

## `slack` <---> `confluence-cloud` Integration

### `slack` <---> `confluence-cloud` Integration Features

1. `slack` ---> `confluence-cloud` ( **instruction** )
    1. `slack >>> workspace >>> ${channel}` && `slack >>> workspace >>> apps >>> jira`：发出`/confluence xxx`指令对`confluence-cloud`进行操作（输入`/confluence help`查看帮助）
    1. `slack >>> workspace >>> ${channel}` && `slack >>> workspace >>> apps >>> jira`：对接受到的`notification`进行操作（点击`message`的`...`选择操作类型）
1. `slack` <--- `confluence-cloud` ( **notification** )
    1. `slack >>> workspace >>> apps >>> jira`中订阅`confluence-cloud`的`notification`
    1. `slack >>> workspace >>> ${channel}`中订阅`confluence-cloud project`的`notification`

### `slack` <---> `confluence-cloud` Integration Steps

1. linking `slack workspace` and `confluence-cloud site`：在`slack >>> workspace >>> apps`搜索并安装`confluence-cloud`，按指引关联`slack workspace`和`confluence-cloud site`
1. linking `slack channel` and `confluence-cloud space`
    - 方法01：`slack >>> workspace >>> channel` || `slack >>> workspace >>> apps >>> jira`输入`/confluence connect`将`slack channel`和`confluence-cloud space`进行关联
    - 方法02：`confluence-cloud >>> ${space} >>> space settings >>> integrations >>> slack notification`

>1. [atlassian.com. Use Slack and Confluence together.](https://support.atlassian.com/confluence-cloud/docs/use-slack-and-confluence-together/)

## `slack` ---> `gitlab` Integration

### `slack` ---> `gitlab` Integration Features

>1. [gitlab.com. Slack slash commands.](https://docs.gitlab.com/ee/user/project/integrations/slack_slash_commands.html)

### `slack` ---> `gitlab` Integration Steps

>1. [gitlab.com. GitLab Slack application.](https://docs.gitlab.com/ee/user/project/integrations/gitlab_slack_application.html)
>1. [gitlab.com. Configure GitLab and Slack.](https://docs.gitlab.com/ee/user/project/integrations/slack_slash_commands.html#configure-gitlab-and-slack)
