# Glossary

## Shared Branch

涉及两人或两人以上操作的分支。

`shared branch`一般情况下是`permanent branch`，在`pair programming`情况下，`temporary branch`也要通过`remote`进行协同，此时，`temporary branch`（或部分`temporary branch`）也是`shared branch`。

## Long-Term Branch

长期存在的分支，即`merge`后不删除的分支。

`long-term branch`是`shared branch`。

## Permanent Branch

`permanent branch`常设分支

the same meaning of `long-term branch`, and it is much better than `long-term branch` for its semantics.

## Private Branch

仅由一人操作的分支。

## temporary Branch

临时存在的分支，即`merge`后可删除的分支。

## Merge Request

## Pull Request

the same mean as `merge request`, which used in `github`.
