# Documentation of `yat-pipelines`

```bash {.line-numbers cmd=true output=text hide=true run_on_save=true}
tree -L 2 -d .
```

```bash {.line-numbers}
.
├── yat-conventions
│   └── yat-coding-style
├── yat-flows
│   ├── yat-branching-model
│   ├── yat-commit-model
│   └── yat-issue-model
└── yat-guides
    ├── yat-courses
    ├── yat-integrations
    ├── yat-roles
    └── yat-tools
```
