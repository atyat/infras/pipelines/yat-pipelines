# yat-issue-model

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [Principals](#principals)
2. [Cautions](#cautions)
3. [The Model](#the-model)
    1. [Issue Life-Cycle](#issue-life-cycle)
    2. [Issue Split and Meanings](#issue-split-and-meanings)
        1. [`epic`](#epic)
        2. [`story`](#story)
        3. [`sub-task`](#sub-task)
    3. [Issue Statuses and Transitions](#issue-statuses-and-transitions)
        1. [Statuses Transitions](#statuses-transitions)
            1. [Commit Association Transition](#commit-association-transition)
            2. [Automate Transition](#automate-transition)
    4. [Issue Description Language](#issue-description-language)
    5. [Issue Templates](#issue-templates)

<!-- /code_chunk_output -->

## Principals

1. `issue type`应在对应的抽象层次上使用
1. `issue name`应根据`issue type`和`issue intent`选用对应的`issue name prefix`
1. `issue status`使用`automatic transition`或`status-keyword-driven transition`
    - `automatic transition`指通过行为自动进行状态迁移，如，某个`commit`关联了某个`issue`，则自动将该`issue`迁移到`in progress`
    - `status-keyword-driven`指在某个行为中明确将某个`issue`迁移到指定的状态，如，某个`commit`指明将某个`issue`迁移到`in review`状态，则将该`issue`迁移到`in review`

## Cautions

❗ ❗ ❗

1. 本文中`issue`包含`requirement`的含义，如，"issue split"包含"requirement split"的含义

❗ ❗ ❗

## The Model

### Issue Life-Cycle

start ---> `roadmap` (`jira-cloud`) ---> `prd`(`confluence-cloud`) ---> `issue` start ---> `epic`(`jira-cloud`) ---> `story`/`bug`/`task`(`jira-cloud`) ---> `sub-task`(`jira-cloud`, optional) ---> `branch`(`gitlab`) ---> `push`(`gitlab`) ---> `ci testing`(`gitlab-ci`) ---> `merge request` && `code review` (`gitlab`) ---> `merge`(`gitlab`) ---> `issue` end

### Issue Split and Meanings

`issue split`通过`epic ---> story ---> sub-task`三个层级进行拆分：

1. `epic`与`story`对应`user requirement`层级，`sub-task`对应`function requirement`层级
1. `epic`与`usecase`的粒度相近，`story`的粒度在一个`sprint`内可完成

#### `epic`

1. `epic`：规划`issue`或巨型`issue`（超过一个`sprint`）<!--`epic`仅在`jira`或其他项目 管理软件内部使用-->
    1. **application special purposes**
        1. `${application-key}-${epic-name}`：与`issue-key`相似但不相同的`keyword`，一般情况下，`issue-key`比`application-key`更简略，例如，本仓库，`issue-key`应为`YL`，`application-key`应为`YATPL`，<!--建议`application-key`作为`issue-key`的一部分-->
    1. **general purposes**
        1. `infra`：基础架构，包含`repo`，`cicd pipeline`等
        1. `architectural`：软件架构
        1. `patch`：研发过程中的小细节修订

<!--
    1. `plan-`：规划`epic`
    1. `mass-`：巨型`epic`
-->

#### `story`

1. `story`：需求类`issue`，来自质量属性的需求，对应用户需求/外部质量需求及其优化需求、内部质量需求及其优化需求
    1. `feat-`：用户需求/外部质量需求
    1. `improve-`：用户需求/外部质量需求的优化需求
    1. `qty-`：内部质量需求，直接体现在软件代码上的内部质量需求
    1. `qtyimprove-`：内部质量需求的优化需求
    1. `misc-`：其他
1. `bug`：缺陷跟综类`issue`
    1. `bugfix-`：交付前发现的缺陷的修复需求
    1. `hotfix-`：交付后发现的缺陷的修复需求
1. `task`：任务类`issue`，具体的工作任务
    1. `task-repo-`：仓库配置，包括`pipelines`、`editorconfig`、`license`、`ignore`、`convention`等
    1. `task-refactor-`：重构
    1. `task-ci-`：自动化测试任务
    1. `task-uat-`：部署在`UAT`/`RC`环境上的人工测试任务
    1. `task-cd-`：`deliver` and `deploy`
    1. `task-mgt-`: 项目管理任务
    1. `task-misc-`：其他

#### `sub-task`

对`story`和`task`的细化拆分，在前缀之前添加`st-`前缀，如：`feat-`为`st-feat-`，`task-repo-`为`st-task-repo-`。

### Issue Statuses and Transitions

1. created ---> `todo`：
1. `in progress`：
1. `in staging`：从个体开发进入到团队`review`，合并到`default branch`后迁移到`done`
1. --- (`MR` to `default branch`) ---> `done` ---> closed：

#### Statuses Transitions

<!-- TODO: try workflow automation -->

##### Commit Association Transition

`commit`是`developer`的个体行为，`merge request`/`pull request`能否通过是团队行为（需要`code review`），因此，

❗ ❗ ❗

1. **禁止在`commit message`中使用会触发`jira issue`关闭的`#keyword`（使用不会触发行为的`->done`代替），应在`merge request`/`pull request`中使用**
1. `jira-cloud`的`smart commits: <issue-key> #<transition_name>`和`gitlab`的`automatic issue closing: closes <issue-key>`都只在`merge request`到`default branch`时才会触发状态迁移

❗ ❗ ❗

`yat-commit-model`的`issue status transition`使用流程如下：

1. `todo` ---> `in progress`：在`commit message`中使用`#in-progress`将状态迁移到`in progress`
1. {`todo` | `in progress`} ---> `in review`：在`commit message`中使用`#in-review`将状态迁移到`in review`
1. {`todo` | `in progress` | `in review`} ---> `in stage`：在`commit message`中使用`#in-review`将状态迁移到`in review`
1. `*` ---> `done`/`close`
    1. 在`commit message`中使用`->done`标识已完成该`jira issue`（不会触发`jira`实际的状态迁移）
    1. 在`merge request` to `default branch`时使用`Resolves`或`Closes`关闭`not-bug issue`，使用`Fixes`关闭`bug issue`（ **仅当合并至`default branch`时发生实际的状态迁移** ）

>1. [KEV ZETTLER. Automatically transition issue status in Jira and GitLab.](https://www.atlassian.com/devops/automation-tutorials/jira-automation-rule-to-transition-issues)
>1. [gitlab.com. Close Jira issues in GitLab.](https://docs.gitlab.com/ee/integration/jira/issues.html#close-jira-issues-in-gitlab)
>1. [atlassian.com. Use Smart Commits.](https://support.atlassian.com/bitbucket-cloud/docs/use-smart-commits/)
>1. [atlassian.com. Configure workflow triggers.](https://support.atlassian.com/jira-cloud-administration/docs/configure-workflow-triggers/)

##### Automate Transition

### Issue Description Language

1. `user story`: `as ... (role), i want to ... (actions/functions), so that ... (goals/values)`
1. `job story`: `when ... (situation), i want to ... (motivations), so i can ... (expects)`

### Issue Templates

<!--

### Automatic Issue Closing

`gitlab`、`github`等`git-platform`都可以通过`keyword`自动关闭`issue`。

1. `gitlab`: [gitlab.com. Default closing pattern.](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#default-closing-pattern)
    1. `close`
    1. `fixed`
    1. `resolved`
    1. `implemented`
1. `github`: [github.com. Linking a pull request to an issue using a keyword.](https://docs.github.com/en/issues/tracking-your-work-with-issues/linking-a-pull-request-to-an-issue#linking-a-pull-request-to-an-issue-using-a-keyword)
    1. `close`
    1. `fixed`
    1. `resolved`

#### `Automatic Issue Closing Keyword`使用场合

`commit`是每个`developer`的个体行为，`merge request`/`pull request`是否通过是组织行为（需要`code review`），因此，

❗ ❗ ❗ **禁止在commit message中使用`automatic issue closing keyword`，应在`merge request`/`pull request`中使用** ❗ ❗ ❗

### `Automatic Issue Closing Keyword`使用及其语义

`fixed`，`resolved`，`implemented`统一使用过去时态。

1. `close`：❗ **不使用** ❗，因为`close`仅表示关闭这个动作，但没有表示进一步的语义
1. `fixed`：**修复了`bug`**
1. `resolved`：**实现了`feature`（`github`）或`improvement`（`github`）或完成了`task`**
1. `implemented`：**实现了`feature`（`gitlab`）或`improvement`（`gitlab`）**

-->
