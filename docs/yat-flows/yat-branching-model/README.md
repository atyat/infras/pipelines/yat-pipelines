# yat-branching-model

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [Principals](#principals)
2. [Cautions](#cautions)
3. [The Model](#the-model)
    1. [Branch Types](#branch-types)
    2. [Issue-Branch-Mapping Strategy](#issue-branch-mapping-strategy)
    3. [Working-Branch Strategy](#working-branch-strategy)
        1. [Common Strategy](#common-strategy)
        2. [`main-power-working-branch strategy`](#main-power-working-branch-strategy)
        3. [`equal-collaborate-working-branch strategy`](#equal-collaborate-working-branch-strategy)
    4. [Semantic Release](#semantic-release)

<!-- /code_chunk_output -->

## Principals

1. 创建分支与合并分支在一个项目中应始终依照一致的`working-branch strategy`和`issue-branch-mapping strategy`
1. 涉及共享分支的合并应在`remote`进行`merge request`，不能在`local`进行`merge`
1. 临时分支之间相互独立，即不能直接`merge`或`merge request`，必须通过常设分支进行间接同步
1. `default branch`和`publish branch`不可进行`push`，只能通过`merge request`进行更新

## Cautions

1. 将`gitlab`的`${repo} >>> settings >>> general >>> merge method`设置成`Merge commit with semi-linear history`

>1. [gitlab.com. Merge methods.](https://docs.gitlab.com/ee/user/project/merge_requests/methods/index.html)

## The Model

基于`git-flow`进行修改：

1. 去除`release`预发布临时分支，`develop merge request to main`时进行完整的`test`和`code review`（包括`automate testing` ---> `manual testing` ---> `code review`）
1. 基于`remote`场景增加`issue-branch-mapping strategy`和`working-branch strategy`
    1. `issue-branch-mapping strategy`：`issue`和`branch`的映射对应关系
    1. `working-branch strategy`：开发人员日常工作所处的分支

### Branch Types

1. 2个常设分支：
    1. `main`：`default branch`, `publish branch`
    1. `develop`：`developing branch`
1. n个临时分支：
    1. `non-hotfix-branch`：从`develop`创建分支、合并到`develop`分支
    1. `hotfix-branch`：从`main`创建分支、合并到`main`分支和`develop`分支

### Issue-Branch-Mapping Strategy

临时分支均按`jira`的`jira issue hierarchy`中的某一层次创建分支，例如，不能即按`epic`创建分支，又按`story`创建分支：

1. `epic-branching`：分支按`epic`创建分支
1. `story-branching`：分支按`story`（包括`stroy`、`bug`、`task`）创建分支

❗ 不应该按`sub-task`创建分支 ❗

>1. [atlassian.com. Reference issues in your development work.](https://support.atlassian.com/jira-software-cloud/docs/reference-issues-in-your-development-work/)

### Working-Branch Strategy

#### Common Strategy

1. 每个`commit`必须关联并且仅关联一个`issue`
1. 每个`commit`之前必须`git rebase origin/${based-branch}`，建议经常`git rebase`

#### `main-power-working-branch strategy`

- `main-power`/`main-programmer`/`repo-maintainer`
    1. 工作于`epic-branching`临时分支（`bugfix`和`hotfix`工作于`story-branching`临时分支）
    1. 每个`commit`必须关联并且仅关联一个`epic`（`bugfix`和`hotfix`关联一个`story`），建议关联一个`story`
- `others`
    1. 工作于`story-branching`临时分支
    1. 每个`commit`必须关联并且仅关联一个`story`

#### `equal-collaborate-working-branch strategy`

1. 工作于`story-branching`临时分支
1. 每个`commit`必须关联并且仅关联一个`story`

### Semantic Release

>1. [to-be-continuous/semantic-release.](https://gitlab.com/to-be-continuous/semantic-release)
