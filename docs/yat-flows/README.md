# `yat-flow`

## Contents of `yat-flow`

```bash {.line-numbers}
.
├── yat-branching-model         分支模型：branch type、branching-merge策略、issue-branch映射
├── yat-commit-model            提交模型：commit message规范、issue status自动迁移
└── yat-issue-model             issue模型：issue类型、issue层次、issue命名
```
