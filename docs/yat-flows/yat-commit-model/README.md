# yat-commit-model

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [`yat-commit-model` Principals](#yat-commit-model-principals)
2. [Conventional Commits](#conventional-commits)
    1. [Conventional Commits Assistant Tools](#conventional-commits-assistant-tools)

<!-- /code_chunk_output -->

## `yat-commit-model` Principals

1. 每个`commit`必须使用`conventional commits`
1. 每个`commit`必须关联并且仅关联一个`story`
1. 每个`merge request`必须关联并且仅关联一个`epic`

## Conventional Commits

use `git cz`.
<!--TODO:-->

### Assistant Tools

1. [KnisterPeter/vscode-commitizen](https://github.com/KnisterPeter/vscode-commitizen)

## Semantic Merge Request

## Futhermore
