# `yat-pipelines`

`yat-pipelines(Yet Another Try Pipelines)`是一个端到端的、结合`DevOps(Development and Operations，开发运维一体化)`和`Cloud Native(云原生)`的软件研发自动化基础架构，`yat-pipelines`可用于以下场景：

1. **参考实现** ：提供了一个软件研发组织参考的开发运维流程参考实现，软件研发组织可在该实现的基础上结合自身的场景修改形成适合的`pipelines`
1. **教学素材** ：提供软件工程专业（或相关专业）的教学素材，可将`yat-pipelines`涉及的工具及其蕴含的理念应用到相应的课程，也可将整个或其中一段`pipelines`应用到《综合实践》课程中

## DevOps与Cloud Native

DevOps是Development和Operations的组合词，是一组过程、方法与系统的统称，用于促进开发、技术运营和质量保障部门之间的沟通、协作与整合。研发运营一体化是将应用的需求、开发、测试、部署和运营统一起来，实现敏捷开发、持续交付和应用运营的无缝集成。DevOps不是单一的技术或者工具，甚至不只是一个流程，它可以被理解为一系列可以高速、高质量进行软件开发的工具链，这种模式不仅提高了软件开发的效率和最终产品的表现，更是现代IT企业协作及共享文化的体现和应用。

DevOps是一个软件工程思想的集大成者，包括敏捷开发思想、精益开发思想等，也是一个方法、工具、技术的集大成者，它在一种在开源生态环境下，将各种最佳实践（Best Practice）的方法、工具组合串联起来形成的有机整体，从而达到持续优化的效果。DevOps与开源生态、云计算、云原生等当前最热门的技术词汇紧密相联。DevOps与敏捷开发、CI/CD（Continuous Integration/Continuous Delivery/Continuous Deploy，持续集成/持续交付/持续部署）的关系如图所示：

![agile, cicd and devops](./.assets/diagram/agile_and_devops.png)

DevOps最初起缘于2009年， 2014年左右随着国内互联网企业实践DevOps而逐渐推广到其他领域，2017年底，中国信通院正式立项《DevOps标准体系及能力成熟度模型》。可以说，DevOps已经获得政府、市场的认可，成为一个主流，并且仍在不断地前进和发展中。根据2021年的调研报告显示，绝大部分企业均已开始实施或计划实施DevOps，仅有6.29%的企业不知道或不打算引入DevOps。

DevOps方法的应用与容器技术发展息息相关，容器技术的推广提高了开发和运维的便捷度，并为开发人员进入运维提供了通道，根据调查表明，企业对容器云技术的应用深度对DevOps的引入与否有重要的相关关系，对容器技术的应用越趋于成熟，越有可能开展DevOps方法的实践，另一方面，容器技术是云原生的核心之一，云原生则为DevOps大展身手提供了广阔的平台。

## `yat-pipelines`的SWOT

### 优点

1. 自动化：除必要的需求发起、代码提交、人工卡点外，尽可能自动化（自动化基础设施、自动化配置、自动化构建、自动化测试、自动化部署、自动化监控）
1. 端到端：从需求到运维监控的端到端研发过程全链路
1. 可度量：通过工具自动化和工程流程规范，将数据记录，进而进行度量
1. 可视化：通过可视化方式显现状态和分析结果

### 缺点

1. 账号体系与`SSO`：缺少与各工具同步账号以及实现`SSO`的功能，实现同步账号需要研发账号系统并与各工具对接，实现`SSO`需要各工具的商业授权

## `yat-pipelines`的架构与规范

### 架构

![`yat-pipelines` architecture](./.assets/diagram/architecture.png)

### 规范

`yat-pipelines`涉及的工程规范：

1. `file hierarchy convention`：目录结构约定
1. `code style`：代码风格/代码规范
1. `issue model`：需求规范/需求模型
1. `branching model`：分支规范/分支模型
1. `commit specification`：提交信息规范

## `yat-pipelines`的用户角色与用户场景User Roles and User Scenario

### 用户角色

|用色|描述|
|:--|:--|
|团队内成员/member|所有团队内的成员|
|团队外人员/public|所有团队外的人员|
|团队研发人员/r&d|团队内的开发人员，包括需求工程师、架构工程师、开发工程师、测试工程师、运维工程师|
|团队核心人员/leader|团队内具有`merge to main`权限的人员（即，`gitlab repo`中的`maintainer`）|
|产品负责人/owner|产品的负责人/开发经理，在团队核心人员中|

### 用户场景

|场景|用户角色|工具|前置条件|使用方法|
|:--|:--|:--|:--|:--|
|我想头脑风暴|团队内成员|`slack`|无|在`slack channel`中聊天|
|我想提出`story`|团队内成员|`slack`|无|在`slack channel`中使用`slack-jira integration`提出需求（`jira issue`创建，非产品负责人提出需求时不分派`assignee`）|
|我想提出`story`|团队外人员|`gitlab`|无|在`gitlab`中新建`issue`（经讨论后，由团队内成员在`slack`中正式提出需求或关闭`issue`）|
|我想讨论`story`|团队内成员|`slack`|`jira issue`已创建|在`slack channel`的`jira issue thread`中讨论（`slack`将自动同步到`jira`）|
|我想详细描述`story`|团队内成员|`confluence` && `jira`|`jira issue`已创建|在`confluence`中新建`page`，详细描述`story`，并在`jira issue`中联接`confluence page`
|我想讨论需求|团队外人员|`gitlab`|`gitlab issue`已创建|在`gitlab issues`中讨论|
|我想规划产品需求和控制进度|产品负责人|`jira`|`jira issue`已创建|使用`scrum`进行敏捷迭代，使用`kanban`跟踪进度|
|我想分派开发任务|产品负责人|`jira`|`jira issue`已创建|在`jira`中正式分派`assignee`|
|我想执行开发任务|团队开发人员|`jira` && `local`|`jira issue`已分派|在`jira`通过`jira-gitlab integration`创建临时分支，在`local`通过`git fetch <remote>`并`git checkout -b <branch> <remote>/<branch>`|
|我想提交开发结果|团队开发人员|`local`|`jira issue`已分派|`git cz`（关联`jira issue id`） && `git push <remote>`并触发`gitlab cicd`完成自动化测试和人工测试|
|我想合并开发结果|团队开发人员|`gitlab`|`gitlab cicd pass`|在`gitlab`中提出`merge request to dev`|
|我想批准开发结果|产品负责人|`gitlab`|`code review`完成|在`gitlab`中批准`merge request to dev`|
|我想上线开发结果|团队核心人员|`gitlab`|`dev`已完成`merge`|在`gitlab`中提出`dev merge request to main`|
