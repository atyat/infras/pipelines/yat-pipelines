#!/usr/bin/env bash

if [[ -z $1 ]]; then
    echo -e "usage:\t$0 <registration_token>"
    echo -e "\tthe registration_token can found in 'group or project >>> settings >>> ci/cd >>> runners' ..."
    exit
fi

token=$1

docker compose up -d && \
    docker compose exec gitlab-runner-general \
        gitlab-runner register \
            --non-interactive \
            --url https://gitlab.com/ \
            --registration-token ${token} \
            --executor docker \
            --description "general gitlab-runner with maven:3.8.6" \
            --tag-list "yast" \
            --docker-image "maven:3.8.6-amazoncorretto-17" \
            --docker-volumes /var/run/docker.sock:/var/run/docker.sock