# gitlab-runner-15.2.1-maven

## General Config

1. `image`: `gitlab/gitlab-runner:ubuntu-v15.2.1`
1. `default docker image`: `maven:3.8.6-amazoncorretto-17`

## Usage

1. `tag-list`: change your `tag-list` in [./start_gitlab-runner.sh](./start_gitlab-runner.sh)
1. `token`：change your `token` in [./start_gitlab-runner.sh](./start_gitlab-runner.sh) or run `./start_gitlab_runner.sh ${your_token}` or run `echo ${your_token} > .gitlab-runner_token && ./start_gitlab_runner.sh $(cat .gitlab-runner_token)`
