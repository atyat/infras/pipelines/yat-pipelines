#!/usr/bin/env bash

docker compose exec gitlab-runner-general gitlab-runner unregister --all-runners && \
    docker compose down --remove-orphans